// console.log("Hello world!")

// 1.
// Create a trainer object
// initialize/add the following trainer object properties:
// Name (String)
// Age (Number)
// Pokemon (Array)
// Friends (Object with Array values for properties)
// Initialize the trainer object method named ‘talk’ that prints out the message Pikachu! I choose you!
// Access the name of the trainer through dot notation
// Access the pokemons through square bracket notation.
// Invoke/call the trainer talk object method.

let trainer = {
	Name: "Ash Ketchum",
	Age: 10,
	Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	Friends: {
		Hoen:["May", "Max"],
		Kanto: ["Brock", "Misty"]
	}
}

trainer.talk = function() {
	console.log(this['Pokemon'][0] + "! I choose you!")
}

trainer.talk()

// 2.
// 	a. 	Create a constructor for creating a pokemon with the following properties:
// 		name (Provided as an argument to the contructor)
// 		level (Provided as an argument to the contructor)
// 		health : Create an equation (health must be 2 times the level value)
// 		attack: Create an equation (attack must be the same value as the level)
// 	b.	Create a several pokemon objects from the constructor with varying name and level properties.


function Pokemon(name,level) {
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;

	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);

		target.health -= this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health);

		if(target.health <= 0) {
			target.faint();
		}
	}

	this.faint = function() {
		console.log(this.name + " fainted")
	}
}

let pikachu = new Pokemon ("Pikachu", 12);
let geodude = new Pokemon ("Geodude", 8);
let mewtwo = new Pokemon ("Mewtwo", 100);

// 3.
// 	a.	Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
// 	b.	Create a faint method that will print out a message of targetPokemon has fainted.
// 	c.	Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
// 	d.	Invoke the tackle method of one pokemon object to see if it works as intended.

// 4.
// a.	Invoke the tackle method of one pokemon object to see if it works as intended.
// b.	Create a git repository named S23.
// c.	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// d.	Add the link in Boodle.


geodude.tackle(pikachu)
mewtwo.tackle(geodude)
pikachu.tackle(geodude)
pikachu.tackle(mewtwo)